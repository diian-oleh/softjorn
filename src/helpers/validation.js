// import { helpers } from 'vuelidate/lib/validators'

export function phoneMask(param) {
  const reg = /^\+380\d{3}\d{2}\d{2}\d{2}$/;
  return reg.test(param);
}
